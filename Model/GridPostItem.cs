﻿using OntologyAppDBConnector.Base;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module;

namespace WordpressSyncModule.Model
{
    public class GridPostItem : NotifyPropertyChange
    {
        private PostItem postItem;

        public string Guid
        {
            get { return postItem.Guid; }
            set
            {
                postItem.Guid = value;
            }
        }

        public int Id
        {
            get { return int.Parse(postItem.Id); }
            set
            {
                postItem.Id = value.ToString();
            }
        }

        public string Title
        {
            get { return postItem.Title; }
            set
            {
                postItem.Title = value;
            }
        }

        public DateTime Created
        {
            get { return postItem.Created; }
            set
            {
                postItem.Created = value;
            }
        }

        public string Status
        {
            get { return postItem.Status; }
            set
            {
                postItem.Status = value;
            }
        }

        public string Type
        {
            get { return postItem.Type; }
            set
            {
                postItem.Type = value;
            }
        }

        public string Name
        {
            get { return postItem.Name; }
            set
            {
                postItem.Name = value;
            }
        }


        public string Author
        {
            get { return postItem.Author; }
            set
            {
                postItem.Author = value;
            }
        }

        public string Parent
        {
            get { return postItem.Parent; }
            set
            {
                postItem.Parent = value;
            }
        }

        public string Link
        {
            get { return postItem.Link; }
            set
            {
                postItem.Link = value;
            }
        }

        private string termItems;
        public string TermItems
        {
            get { return termItems; }
            set
            {
                termItems = value;
            }
        }

        public GridPostItem(PostItem postItem)
        {
            this.postItem = postItem;
            this.postItem.PropertyChanged += PostItem_PropertyChanged;
            TermItems = string.Join(", ", postItem.TermItems.Select(termItem => termItem.Name));
        }

        private void PostItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e.PropertyName);
            TermItems = string.Join(", ", postItem.TermItems.Select(termItem => termItem.Name));
        }
    }
}
