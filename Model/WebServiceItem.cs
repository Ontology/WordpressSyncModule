﻿using OntologyAppDBConnector.Attributes;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordpressSyncModule.Notifications;

namespace WordpressSyncModule.Model
{
    public class WebServiceItem : NotifyPropertyChange
    {
        private string guid;
        public string Guid
        {
            get { return guid; }
            set
            {
                guid = value;
                RaisePropertyChanged(NotifyChanges.WebServiceItem_Guid);
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged(NotifyChanges.WebServiceItem_Name);
            }
        }

        
        private clsOntologyItem userItem;
        public clsOntologyItem UserItem
        {
            get { return userItem; }
            set
            {
                userItem = value;
                RaisePropertyChanged(NotifyChanges.WebServiceItem_UserItem);
            }
        }

        private clsOntologyItem urlItem;
        public clsOntologyItem UrlItem
        {
            get { return urlItem; }
            set
            {
                urlItem = value;
                RaisePropertyChanged(NotifyChanges.WebServiceItem_UrlItem);
            }
        }

        public string Password
        {
            get; set;
        }
    }
}
