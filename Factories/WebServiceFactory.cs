﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Attributes;
using OntologyClasses.AbstractClasses;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WordpressSyncModule.Model;

namespace WordpressSyncModule.Factories
{
    public class WebServiceFactory 
    {
        private clsLocalConfig localConfig;
        public OntologyModDBConnector dbReader_WebServices;
        public OntologyModDBConnector dbReader_Users;
        public OntologyModDBConnector dbReader_Urls;

        public List<WebServiceItem> WebServices { get; private set; }

        public WebServiceFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }


        public clsOntologyItem GetData_BaseConfig()
        {

            var searchWebServices = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = localConfig.OItem_object_baseconfig.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_belonging.GUID,
                    ID_Parent_Other = localConfig.OItem_class_web_service.GUID
                }
            };

            var result = dbReader_WebServices.GetDataObjectRel(searchWebServices);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

           

            var searchUsers = dbReader_WebServices.ObjectRels.Select(webService => new clsObjectRel
            {
                ID_Object = webService.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_secured_by.GUID,
                ID_Parent_Other = localConfig.OItem_class_user.GUID
            }).ToList();

            result = dbReader_Users.GetDataObjectRel(searchUsers);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            var searchUrls = dbReader_WebServices.ObjectRels.Select(webService => new clsObjectRel
            {
                ID_Object = webService.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_accessible_by.GUID,
                ID_Parent_Other = localConfig.OItem_class_url.GUID
            }).ToList();

            result = dbReader_Urls.GetDataObjectRel(searchUrls);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            

            WebServices = (from webService in dbReader_WebServices.ObjectRels
                           join userItem in dbReader_Users.ObjectRels on webService.ID_Other equals userItem.ID_Object
                           join urlItem in dbReader_Urls.ObjectRels on webService.ID_Other equals urlItem.ID_Object
                           select new WebServiceItem
                           {
                               Guid = webService.ID_Other,
                               Name = webService.Name_Other,
                               UrlItem = new clsOntologyItem
                               {
                                   GUID = urlItem.ID_Other,
                                   Name = urlItem.Name_Other,
                                   GUID_Parent = urlItem.ID_Parent_Other,
                                   Type = urlItem.Ontology
                               },
                               UserItem = new clsOntologyItem
                               {
                                   GUID = userItem.ID_Other,
                                   Name = userItem.Name_Other,
                                   GUID_Parent = userItem.ID_Parent_Other,
                                   Type = urlItem.Ontology
                               }
                           }).ToList();

            //WebServices.ForEach(webService =>
            //{
            //    var password = localConfig.SecurityWork.decode_Password(webService.UserItem);

            //    if (password != null)
            //    {
            //        webService.Password = password;
            //    }
            //});

            return result;

        }

        private void Initialize()
        {
            dbReader_WebServices = new OntologyModDBConnector(localConfig.Globals);
            dbReader_Users = new OntologyModDBConnector(localConfig.Globals);
            dbReader_Urls = new OntologyModDBConnector(localConfig.Globals);

        }
    }
}
