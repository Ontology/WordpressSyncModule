﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module;
using WordpressSyncModule.Model;

namespace WordpressSyncModule.Factories
{
    public class PostsFactory
    {
        public bool WriteBlogJson(SessionFile sessionFile, List<GridPostItem> postItems)
        {
            using (sessionFile.StreamWriter)
            {
                using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                {
                    jsonWriter.WriteStartArray();

                    postItems.ForEach(postItem =>
                    {
                        jsonWriter.WriteStartObject();

                        jsonWriter.WritePropertyName("Guid");
                        jsonWriter.WriteValue(postItem.Guid);

                        jsonWriter.WritePropertyName("Id");
                        jsonWriter.WriteValue(postItem.Id);

                        jsonWriter.WritePropertyName("Title");
                        jsonWriter.WriteValue(postItem.Title);

                        jsonWriter.WritePropertyName("Created");
                        jsonWriter.WriteValue(postItem.Created);

                        jsonWriter.WritePropertyName("Status");
                        jsonWriter.WriteValue(postItem.Status);

                        jsonWriter.WritePropertyName("Type");
                        jsonWriter.WriteValue(postItem.Type);

                        jsonWriter.WritePropertyName("Name");
                        jsonWriter.WriteValue(postItem.Name);

                        jsonWriter.WritePropertyName("Author");
                        jsonWriter.WriteValue(postItem.Author);

                        jsonWriter.WritePropertyName("Parent");
                        jsonWriter.WriteValue(postItem.Parent);

                        jsonWriter.WritePropertyName("Link");
                        jsonWriter.WriteValue(postItem.Link);

                        jsonWriter.WriteEndObject();
                    });

                    jsonWriter.WriteEndArray();
                }
            }

            return true;
        }
    }
}
