﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordpressSyncModule.Validation;

namespace WordpressSyncModule
{
    public class QueryInfoResultToMessageConverter 
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is BlogQueryInfoValidationResult)) return "";

            var result = "";
            var queryInfo = (BlogQueryInfoValidationResult)value;

            result = queryInfo.ToString();
            result = result.Replace("|", ", ");

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return BlogQueryInfoValidationResult.None;
        }
    }
}
