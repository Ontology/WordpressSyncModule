﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordpressSyncModule.Validation;

namespace WordpressSyncModule.Converter
{
    public class QueryInfoResultToLoginInfoResultConverter 
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is BlogQueryInfoValidationResult)) return PostListInfoValidationResult.Unknown;

            var queryInfoValidationResult = (BlogQueryInfoValidationResult)value;

            var loginInfoValidationResult = PostListInfoValidationResult.None;

            if (queryInfoValidationResult.HasFlag(BlogQueryInfoValidationResult.Unknown)) return PostListInfoValidationResult.Unknown;

            if (queryInfoValidationResult.HasFlag(BlogQueryInfoValidationResult.EmptyPassword)) loginInfoValidationResult |= PostListInfoValidationResult.EmptyPassword;
            if (queryInfoValidationResult.HasFlag(BlogQueryInfoValidationResult.EmptyUrl)) loginInfoValidationResult |= PostListInfoValidationResult.EmptyUrl;
            if (queryInfoValidationResult.HasFlag(BlogQueryInfoValidationResult.EmptyUsername)) loginInfoValidationResult |= PostListInfoValidationResult.EmptyUsername;
            if (queryInfoValidationResult.HasFlag(BlogQueryInfoValidationResult.WrongUriFormat)) loginInfoValidationResult |= PostListInfoValidationResult.WrongUriFormat;
            if (queryInfoValidationResult.HasFlag(BlogQueryInfoValidationResult.WrongUsernameFormat)) loginInfoValidationResult |= PostListInfoValidationResult.WrongUsernameFormat;

            

            return loginInfoValidationResult;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is PostListInfoValidationResult)) return BlogQueryInfoValidationResult.Unknown;
            

            var loginInfoValidationResult = (PostListInfoValidationResult)value;

            var queryInfoValidationResult = BlogQueryInfoValidationResult.None;

            if (loginInfoValidationResult.HasFlag(PostListInfoValidationResult.NoBlogItem)) return BlogQueryInfoValidationResult.Unknown;
            if (queryInfoValidationResult.HasFlag(PostListInfoValidationResult.Unknown)) return PostListInfoValidationResult.Unknown;

            if (loginInfoValidationResult.HasFlag(PostListInfoValidationResult.EmptyPassword)) queryInfoValidationResult |= BlogQueryInfoValidationResult.EmptyPassword;
            if (loginInfoValidationResult.HasFlag(PostListInfoValidationResult.EmptyUrl)) queryInfoValidationResult |= BlogQueryInfoValidationResult.EmptyUrl;
            if (loginInfoValidationResult.HasFlag(PostListInfoValidationResult.EmptyUsername)) queryInfoValidationResult |= BlogQueryInfoValidationResult.EmptyUsername;
            if (loginInfoValidationResult.HasFlag(PostListInfoValidationResult.WrongUriFormat)) queryInfoValidationResult |= BlogQueryInfoValidationResult.WrongUriFormat;
            if (loginInfoValidationResult.HasFlag(PostListInfoValidationResult.WrongUsernameFormat)) queryInfoValidationResult |= BlogQueryInfoValidationResult.WrongUsernameFormat;

            
            

            return loginInfoValidationResult;
        }
    }
}
