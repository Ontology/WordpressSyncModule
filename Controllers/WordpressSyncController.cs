﻿using OntologyAppDBConnector;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Attributes;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module;
using WordpressSyncModule.Factories;
using WordpressSyncModule.Model;
using WordpressSyncModule.Notifications;
using WordpressSyncModule.Translations;
using WordpressSyncModule.Validation;
using System.ComponentModel;
using OntologyClasses.BaseClasses;

namespace WordpressSyncModule.Controllers
{
    public class WordpressSyncController : WordpressSyncViewModel, IViewController
    {
        private clsLocalConfig localConfig;
        private WordPressOntologyEngine wordPressOntologyEngine;
        private WebsocketServiceAgent webSocketServiceAgent;
        private PostsFactory postsFactory = new PostsFactory();
        private TranslationController translationController = new TranslationController();

        private List<WebServiceItem> webservicesPre;
        private DropDownItem emptyItem = new DropDownItem { Id = Guid.NewGuid().ToString(), Value = "" };

        private string webserviceDropDownFile;
        private string weblogDropDownFile;
        private string gridFile;

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public WordpressSyncController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            PropertyChanged += WordpressSyncController_PropertyChanged;
            Initialize();
        }


        private void WordpressSyncController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.MainWindow_SelectedUserBlog)
            {
                ValidateActionSource();
            }
            else if (e.PropertyName == Notifications.NotifyChanges.MainWindow_UserBlogs)
            {
                
                ValidateActionSource();

                if (UserBlogs != null)
                {
                    var dropDownItems = UserBlogs.Select(userBlog => new DropDownItem { Id = userBlog.Guid, Value = userBlog.BlogName }).ToList();
                    dropDownItems.Insert(0, emptyItem);
                    
                    var dataSource = RefreshJqxDataSource(dropDownItems, weblogDropDownFile);

                    if (dataSource == null) return;

                    webSocketServiceAgent.SendCommand("RefreshWebLogs");
                    //JqxDataSource_UserBlogDropDownItems = dataSource;
                    IsEnabled_UserBlogDropDownItems = true;
                }

            }
            else if (e.PropertyName == Notifications.NotifyChanges.MainWindow_UserName)
            {
                ValidateActionSource();
            }
            else if (e.PropertyName == Notifications.NotifyChanges.MainWindow_BaseUrl)
            {
                ValidateActionSource();
            }
            else if (e.PropertyName == Notifications.NotifyChanges.MainWindow_Password)
            {
                ValidateActionSource();
            }

            else if (e.PropertyName == Notifications.NotifyChanges.MainWindow_WebServices)
            {
                if (WebServices != null)
                {
                    var dropDownItems = WebServices.Select(webService => new DropDownItem { Id = webService.Guid, Value = webService.Name }).ToList();
                    dropDownItems.Insert(0, emptyItem);
                    webserviceDropDownFile = Guid.NewGuid().ToString() + ".json";
                    var dataSource = RefreshJqxDataSource(dropDownItems, webserviceDropDownFile);

                    if (dataSource == null) return;

                    JqxDataSource_WebServiceDropDown = dataSource;

                }
                

            }
            else if (e.PropertyName == Notifications.NotifyChanges.MainWindow_PostList)
            {
                var sessionFile = webSocketServiceAgent.RequestWriteStream(gridFile);
                postsFactory.WriteBlogJson(sessionFile, PostList);

                webSocketServiceAgent.SendCommand("RefreshPostGrid");
            }

            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private JqxDataSource RefreshJqxDataSource(List<DropDownItem> dropDownItems, string fileName)
        {
            
            var sessionFile = webSocketServiceAgent.RequestWriteStream(fileName);

            if (sessionFile == null) return null;

            using (sessionFile.StreamWriter)
            {
                sessionFile.StreamWriter.Write(Newtonsoft.Json.JsonConvert.SerializeObject(dropDownItems));

            }

            if (sessionFile.FileUri == null) return null;


            var jqxDataSource = new JqxDataSource(JsonDataType.Json)
            {
                datafields = new List<DataField>
                        {
                            new DataField(DataFieldType.String)
                            {
                                name = "Id"
                            },
                            new DataField(DataFieldType.String)
                            {
                                name = "Value"
                            }
                        },
                id = "Id",
                async = true,
                url = sessionFile.FileUri.AbsoluteUri
            };

            return jqxDataSource;
        }


        private void ValidateActionSource()
        {
            LastQueryInfoValidationResult = ValidationEngine.IsOk_BlogQueryInfo(UserName, Password, BaseUrl);
            IsEnabled_GetBlogs = LastQueryInfoValidationResult == BlogQueryInfoValidationResult.None;
            QueryInfoErrorColor = LastQueryInfoValidationResult == BlogQueryInfoValidationResult.None ? "" : "red";
            LastPostListInfoValidationResult = ValidationEngine.IsOk_PostListInfo(UserName, Password, BaseUrl, SelectedUserBlog);
            IsEnabled_PostList = LastPostListInfoValidationResult == PostListInfoValidationResult.None;
            IsEnabled_UserBlogDropDownItems = LastPostListInfoValidationResult == PostListInfoValidationResult.None;
        }

        public void GetPostList()
        {

            if (SelectedUserBlog != null)
            {
                var result = wordPressOntologyEngine.GetPostList(SelectedUserBlog, BaseUrl, UserName, Password, true);


                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    PostList = new List<GridPostItem>(wordPressOntologyEngine.PostList.Select(postItem => new GridPostItem(postItem)));
                }


            }

        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

        }

      
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "GetBlogs")
                {
                    Show_Loader = true;
                    GetUserBlogs();
                    Show_Loader = false;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "GetPosts")
                {
                    Show_Loader = true;
                    GetPostList();
                    Show_Loader = false;
                }

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                var changedItems = webSocketServiceAgent.ChangedViewItems;

                changedItems.ForEach(changedItem =>
                {
                    if (changedItem.ViewItemId == "webserviceDropdown" && changedItem.ViewItemType == ViewItemType.SelectedIndex.ToString())
                    {
                        int index;
                        if (int.TryParse(changedItem.ViewItemValue.ToString(), out index))
                        {
                            if (index == 0)
                            {
                                UserName = "";
                                BaseUrl = "";
                                Password = "";
                            }
                            else
                            {
                                var selectedItem = WebServices[index - 1];

                                if (selectedItem.Guid == emptyItem.Id) return;

                                UserName = selectedItem.UserItem.Name;
                                BaseUrl = selectedItem.UrlItem.Name;
                                Password = selectedItem.Password;
                            }

                            

                            
                        }
                        
                        
                    }
                    else if (changedItem.ViewItemId == "blogsDropdown" && changedItem.ViewItemType == ViewItemType.SelectedIndex.ToString())
                    {
                        int index;
                        if (int.TryParse(changedItem.ViewItemValue.ToString(), out index))
                        {
                            if (index == 0)
                            {
                                IsEnabled_PostList = false;
                            }
                            else
                            {
                                IsEnabled_PostList = true;
                                SelectedUserBlog = UserBlogs[index - 1];
                            }




                        }
                    }
                });
            }


        }


        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (message.ChannelId == Notifications.NotifyChanges.Channel_DecodePasswords)
            {
                webservicesPre.ForEach(webService =>
                {
                    var userItemWithPassword = message.OItems.FirstOrDefault(oItem => oItem.GUID == webService.UserItem.GUID);

                    if (userItemWithPassword != null)
                    {
                        webService.Password = userItemWithPassword.Additional1;
                    }
                });
                WebServices = webservicesPre;
            }
        }

        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            wordPressOntologyEngine = new WordPressOntologyEngine(localConfig.Globals);
            WebServiceFactory webServiceFactory = new WebServiceFactory(localConfig);
            var result = webServiceFactory.GetData_BaseConfig();
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                webservicesPre = webServiceFactory.WebServices;
                var userItems = webservicesPre.Select(webService => webService.UserItem).ToList();
                if (userItems.Any())
                {
                    var decodeMessage = new InterServiceMessage
                    {
                        ChannelId = Notifications.NotifyChanges.Channel_DecodePasswords,
                        SenderId = webSocketServiceAgent.EndpointId,
                        OItems = userItems
                    };
                    webSocketServiceAgent.SendInterModMessage(decodeMessage);
                }

            }
            weblogDropDownFile = Guid.NewGuid().ToString() + ".json";
            var dataSource = RefreshJqxDataSource(new List<DropDownItem>(), weblogDropDownFile);
            JqxDataSource_UserBlogDropDownItems = dataSource;

            IsEnabled_GetBlogs = true;
            IsEnabled_PostList = true;
            IsEnabled_UserBlogDropDownItems = true;

            IsEnabled_GetBlogs = false;
            IsEnabled_PostList = false;
            IsEnabled_UserBlogDropDownItems = false;

            Label_BaseUrlLabel = translationController.Label_BaseUrl;
            Label_BlogsLabel = translationController.Label_Blogs;
            Label_GetBlogsLabel = translationController.Label_GetBlogs;
            Label_PasswordLabel = translationController.Label_Password;
            Label_PostListLabel = translationController.Label_GetPosts;
            Label_UserNameLabel = translationController.Label_UserName;
            Label_WebservicesLabel = translationController.Label_Webservices;

            gridFile = Guid.NewGuid().ToString() + ".json";
            var sessionFile = webSocketServiceAgent.RequestWriteStream(gridFile);
            postsFactory.WriteBlogJson(sessionFile, new List<GridPostItem>());


            JqxDataSource_Grid = new JqxDataSource(JsonDataType.Json)
            {
                datafields = new List<DataField>
                {
                    new DataField(DataFieldType.String)
                    {
                        name = "Guid"
                    },
                    new DataField(DataFieldType.Number)
                    {
                        name = "Id"
                    },
                    new DataField(DataFieldType.String)
                    {
                        name = "Title"
                    },
                    new DataField(DataFieldType.String)
                    {
                        name = "Created"
                    },
                    new DataField(DataFieldType.String)
                    {
                        name = "Status"
                    }
                    ,
                    new DataField(DataFieldType.String)
                    {
                        name = "Type"
                    },
                    new DataField(DataFieldType.String)
                    {
                        name = "Name"
                    }
                },
                id = "Guid",
                url = sessionFile.FileUri.AbsoluteUri
            };

            ColumnConfig_Grid = new JqxColumnList
            {
                ColumnList = new List<JqxColumnAttribute>
                {
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Checkbox)
                    {
                        text = "Guid",
                        datafield = "Guid",
                        editable = false
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = "Id",
                        datafield = "Id",
                        editable = false
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = "Title",
                        datafield = "Title",
                        editable = false
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = "Created",
                        datafield = "Created",
                        editable = false
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = "Status",
                        datafield = "Status",
                        editable = false
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = "Type",
                        datafield = "Type",
                        editable = false
                    },
                    new JqxColumnAttribute(AlignType.Left, AlignType.Left, ColumnType.Textbox)
                    {
                        text = "Name",
                        datafield = "Name",
                        editable = false
                    }

                }
            };


            webSocketServiceAgent.SendModel();
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {

        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

        }

        public void GetUserBlogs()
        {
            var result = wordPressOntologyEngine.GetUserBlogs(BaseUrl, UserName, Password, true);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                UserBlogs = wordPressOntologyEngine.UserBlogs;
            }
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
