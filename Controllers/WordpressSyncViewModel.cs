﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPress_Module;
using WordPress_Module.Factory;
using WordpressSyncModule.Model;
using WordpressSyncModule.Notifications;
using WordpressSyncModule.Validation;

namespace WordpressSyncModule.Controllers
{
    public class WordpressSyncViewModel : ViewModelBase
    {
       
        
        
        private string userName;
        [ViewModel(Send = true, ViewItemId = "userNameInp", ViewItemClass = ViewItemClass.Input, ViewItemType = ViewItemType.Content)]
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
                
                RaisePropertyChanged(NotifyChanges.MainWindow_UserName);
            }
        }

        private string baseUrl;
        [ViewModel(Send = true, ViewItemId = "baseUrlInp", ViewItemClass = ViewItemClass.Input, ViewItemType = ViewItemType.Content)]
        public string BaseUrl
        {
            get { return baseUrl; }
            set
            {
                baseUrl = value;
                
                RaisePropertyChanged(NotifyChanges.MainWindow_BaseUrl);
            }
        }

        private Uri BaseUri;

        private string password;
        [ViewModel(Send = true, ViewItemId = "passwordInp", ViewItemClass = ViewItemClass.Input, ViewItemType = ViewItemType.Content)]
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                
                RaisePropertyChanged(NotifyChanges.MainWindow_Password);
            }
        }

        private BlogQueryInfoValidationResult lastQueryInfoValidationResult;
        public BlogQueryInfoValidationResult LastQueryInfoValidationResult
        {
            get { return lastQueryInfoValidationResult; }
            set
            {
                lastQueryInfoValidationResult = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LastQueryInfoValidationResult);
            }
        }

        private PostListInfoValidationResult lastPostListInfoValidationResult;
        public PostListInfoValidationResult LastPostListInfoValidationResult
        {
            get { return lastPostListInfoValidationResult; }
            set
            {
                lastPostListInfoValidationResult = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LastPostListInfoValidationResult);
            }
        }

        private List<UserBlog> userBlogs = null;
        public List<UserBlog> UserBlogs
        {
            get { return userBlogs; }
            set
            {
                userBlogs = value;
                
                RaisePropertyChanged(NotifyChanges.MainWindow_UserBlogs);
            }
        }

        private JqxDataSource jqxdatasource_UserBlogDropDownItems;
        [ViewModel(Send = true, ViewItemId = "blogsDropdown", ViewItemClass = ViewItemClass.DropDownList, ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_UserBlogDropDownItems
        {
            get { return jqxdatasource_UserBlogDropDownItems; }
            set
            {
                if (jqxdatasource_UserBlogDropDownItems == value) return;

                jqxdatasource_UserBlogDropDownItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_UserBlogDropDownItems);

            }
        }


        private List<WebServiceItem> webServices = null;
        public List<WebServiceItem> WebServices
        {
            get { return webServices; }
            set
            {
                webServices = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_WebServices);
            }
        }

        private JqxDataSource jqxDataSource_WebServiceDropDown;
        [ViewModel(Send = true, ViewItemId = "webserviceDropdown", ViewItemClass = ViewItemClass.DropDownList, ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_WebServiceDropDown
        {
            get { return jqxDataSource_WebServiceDropDown; }
            set
            {
                if (jqxDataSource_WebServiceDropDown == value) return;

                jqxDataSource_WebServiceDropDown = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_WebServiceDropDown);

            }
        }

        private WebServiceItem selectedWebService;
        public WebServiceItem SelectedWebService
        {
            get
            {
                return selectedWebService;
            }
            set
            {
                selectedWebService = value;
                if (selectedWebService != null)
                {
                    BaseUrl = selectedWebService.UrlItem != null ? selectedWebService.UrlItem.Name : "";
                    UserName = selectedWebService.UserItem != null ? selectedWebService.UserItem.Name : "";
                    Password = selectedWebService.Password;
                }
                    

                RaisePropertyChanged(NotifyChanges.MainWindow_SelectedWebServices);
            }
        }

        private UserBlog selectedUserBlog;
        public UserBlog SelectedUserBlog
        {
            get
            {
                return selectedUserBlog;
            }
            set
            {
                selectedUserBlog = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_SelectedUserBlog);
            }
        }

        

        private bool isEnabled_GetBlogs;
        [ViewModel(Send = true, ViewItemId = "buttonGetBlogs", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_GetBlogs
        {
            get
            {
                return isEnabled_GetBlogs;
            }
            set
            {
                isEnabled_GetBlogs = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_IsEnabled_GetBlogs);
            }
        }

        private bool isEnabled_PostsList;
        [ViewModel(Send = true, ViewItemId = "buttonPostList", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_PostList
        {
            get
            {
                return isEnabled_PostsList;
            }
            set
            {
                isEnabled_PostsList = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_IsEnabled_PostList);

            }
        }

        private string queryInfoErrorColor;
        public string QueryInfoErrorColor
        {
            get { return queryInfoErrorColor; }
            set
            {
                queryInfoErrorColor = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_QueryInfoErrorColor);
            }
        }

        private List<GridPostItem> postList;
        public List<GridPostItem> PostList
        {
            get { return postList; }
            set
            {
                postList = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_PostList);
            }

        }

        private string label_WebservicesLabel;
        [ViewModel(Send = true, ViewItemId = "webserviceLabel", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Content)]
        public string Label_WebservicesLabel
        {
            get { return label_WebservicesLabel; }
            set
            {
                if (label_WebservicesLabel == value) return;

                label_WebservicesLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_WebservicesLabel);

            }
        }

        private string label_BaseUrlLabel;
        [ViewModel(Send = true, ViewItemId = "baseUrlLabel", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Content)]
        public string Label_BaseUrlLabel
        {
            get { return label_BaseUrlLabel; }
            set
            {
                if (label_BaseUrlLabel == value) return;

                label_BaseUrlLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_BaseUrlLabel);

            }
        }

        private string label_UserNameLabel;
        [ViewModel(Send = true, ViewItemId = "userNameLabel", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Content)]
        public string Label_UserNameLabel
        {
            get { return label_UserNameLabel; }
            set
            {
                if (label_UserNameLabel == value) return;

                label_UserNameLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_UserNameLabel);

            }
        }

        private string label_PasswordLabel;
        [ViewModel(Send = true, ViewItemId = "passwordLabel", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Content)]
        public string Label_PasswordLabel
        {
            get { return label_PasswordLabel; }
            set
            {
                if (label_PasswordLabel == value) return;

                label_PasswordLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_PasswordLabel);

            }
        }

        private string label_GetBlogsLabel;
        [ViewModel(Send = true, ViewItemId = "buttonGetBlogs", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Content)]
        public string Label_GetBlogsLabel
        {
            get { return label_GetBlogsLabel; }
            set
            {
                if (label_GetBlogsLabel == value) return;

                label_GetBlogsLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_GetBlogsLabel);

            }
        }

        private string label_BlogsLabel;
        [ViewModel(Send = true, ViewItemId = "blogsLabel", ViewItemClass = ViewItemClass.Label, ViewItemType = ViewItemType.Content)]
        public string Label_BlogsLabel
        {
            get { return label_BlogsLabel; }
            set
            {
                if (label_BlogsLabel == value) return;

                label_BlogsLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_BlogsLabel);

            }
        }

        private string label_PostListLabel;
        [ViewModel(Send = true, ViewItemId = "buttonPostList", ViewItemClass = ViewItemClass.Button, ViewItemType = ViewItemType.Content)]
        public string Label_PostListLabel
        {
            get { return label_PostListLabel; }
            set
            {
                if (label_PostListLabel == value) return;

                label_PostListLabel = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_PostListLabel);

            }
        }

        private bool isenabled_UserBlogDropDownItems;
        [ViewModel(Send = true, ViewItemId = "blogsDropdown", ViewItemClass = ViewItemClass.DropDownList, ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_UserBlogDropDownItems
        {
            get { return isenabled_UserBlogDropDownItems; }
            set
            {
                if (isenabled_UserBlogDropDownItems == value) return;

                isenabled_UserBlogDropDownItems = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_UserBlogDropDownItems);

            }
        }

        private bool show_Loader;
        [ViewModel(Send = true, ViewItemId = "jqxLoader", ViewItemClass = ViewItemClass.Other, ViewItemType = ViewItemType.Other)]
        public bool Show_Loader
        {
            get { return show_Loader; }
            set
            {
                if (show_Loader == value) return;

                show_Loader = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Show_Loader);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemId = "gridBlogentries", ViewItemClass = ViewItemClass.Grid, ViewItemType = ViewItemType.DataSource)]
        public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }

        private JqxColumnList columnconfig_Grid;
        [ViewModel(Send = true, ViewItemId = "gridBlogentries", ViewItemClass = ViewItemClass.Grid, ViewItemType = ViewItemType.ColumnConfig)]
        public JqxColumnList ColumnConfig_Grid
        {
            get { return columnconfig_Grid; }
            set
            {
                if (columnconfig_Grid == value) return;

                columnconfig_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_ColumnConfig_Grid);

            }
        }
    }

}
