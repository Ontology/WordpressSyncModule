﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WordpressSyncModule.Translations
{
    public class TranslationController
    {

        
        public string Title_FileStreamError
        {
            get
            {
                return GetValue("244d7e5cd2e64401bd99a91ba98fda34", "Datei-Fehler!");
            }

        }

        public string Text_FileStreamError
        {
            get
            {
                return GetValue("5682b2ddf0be4d1babeac59245d25a85", "Datei für Datenaustausch konnte  nicht agespeichert werden!");
            }

        }

        public string Title_WebConfigError
        {
            get
            {
                return GetValue("78634024801646ceba093bb8c62f7f0a", "Webserver-Fehler");
            }

        }

        public string Text_WebConfigError
        {
            get
            {
                return GetValue("7d1199b177ba435b8c2bcb917cec913b", "Der Webserver hat einen Fehler verursacht!");
            }

        }

        public string Label_Webservices
        {
            get
            {
                return GetValue("Label_Webservices", "Webservices:");
            }
        }

        public string Label_BaseUrl
        {
            get
            {
                return GetValue("Label_BaseUrl", "Base-Url:");
            }
        }

        public string Label_UserName
        {
            get
            {
                return GetValue("Label_UserName", "Username:");
            }
        }

        public string Label_Password
        {
            get
            {
                return GetValue("Label_Password", "Password:");
            }
        }

        public string Label_GetBlogs
        {
            get
            {
                return GetValue("Label_GetBlogs", "Get Weblogs");
            }
        }

        public string Label_Blogs
        {
            get
            {
                return GetValue("Label_Blogs", "Weblogs:");
            }
        }

        public string Label_GetPosts
        {
            get
            {
                return GetValue("Label_GetPosts", "Get Posts");
            }
        }

        public string GetTranslatedByPropertyName(string propertyName)
        {
            var property = this.GetType().GetProperties().Cast<PropertyInfo>().FirstOrDefault(propItem => propItem.Name == propertyName);

            if (property != null)
            {
                var value = property.GetValue(this);
                return value != null ? value.ToString() : "xxx_Error_xxx";
            }
            else
            {
                return "xxx_Error_xxx";
            }
        }


        private string GetValue(string IdReference, string defaultValue)
        {
            return defaultValue;
        }

    }
}
